The tutorial starts with angular, but doesn't give anyway to actually serve the files.  
Since they are just files, we could probalby just browse directly to the file, but 
since we are inside vagrant, let's give ourselves a little bit nicer tool.  

Let's try node static-server:
  1. Install static-server
     sudo npm -g install static-server
  2. Run statis server
     A. Change into directory you want to server
     B. If you want to change the port, have to run in sudo if you want port 80 due to port protections
     sudo static-server -p 80

To allow external access to MongoDb server
  Note: Normally would not do this unless you also turn authorization on, but since we are in the VM, we are already protected here
   sudo vi /etc/mongod.conf
   Comment out the following line:
     bindIp: 127.0.0.1

If loading the source code instead of going step by step throught the process
  cd <location of angular code>
  npm install
  npm start
     
Later on - after installing express - start the browser (starts on port 3000) (http://33.33.33.110:3000)
  npm start

Tutorial Link:
  https://thinkster.io/tutorials/mean-stack
  
